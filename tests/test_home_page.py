from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
import unittest
from pages.home import Home

class TestHomePage(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.driver.get("http://localhost")

    def test_home_page(self):
        home_page = Home(self.driver)
        assert home_page.title_exists()

    def tearDown(self):
        self.driver.close()

