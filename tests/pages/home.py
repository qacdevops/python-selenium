from pages import Base

class Home(Base):
    def title_exists(self):
        title = self.driver.find_element_by_xpath('/html/body/h1')
        return 'Welcome to nginx!' in title.text

